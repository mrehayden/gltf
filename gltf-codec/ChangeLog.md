# Changelog for `gltf-codec`

## [0.1.0.4] - 2022-03-23

* Compatibility with GHC 9.2, aeson-2, text-2.

## [0.1.0.3] - 2021-01-22

* Fixed `TextureInfo` parsing.

## [0.1.0.2] - 2021-01-22

* Added `fromByteString` for GlTF and GLB data.

## [0.1.0.1] - 2020-06-21

* Added pattern synonyms for newtyped "allowed values".

## [0.1.0.0] - 2020-06-07

* Initial import.

[0.1.0.4]: https://gitlab.com/dpwiz/gltf/tree/v0.1.0.4
[0.1.0.3]: https://gitlab.com/dpwiz/gltf/tree/v0.1.0.3
[0.1.0.2]: https://gitlab.com/dpwiz/gltf/tree/v0.1.0.2
[0.1.0.1]: https://gitlab.com/dpwiz/gltf/tree/v0.1.0.1
[0.1.0.0]: https://gitlab.com/dpwiz/gltf/tree/v0.1.0.0
