module Codec.GlTF.Root where

import Codec.GlTF.Prelude

import Codec.GlTF.Asset (Asset)
import Codec.GlTF.Accessor (Accessor)
import Codec.GlTF.Animation (Animation)
import Codec.GlTF.Buffer (Buffer)
import Codec.GlTF.BufferView (BufferView)
import Codec.GlTF.Camera (Camera)
import Codec.GlTF.Image (Image)
import Codec.GlTF.Material (Material)
import Codec.GlTF.Mesh (Mesh)
import Codec.GlTF.Node (Node)
import Codec.GlTF.Sampler (Sampler)
import Codec.GlTF.Scene (Scene)
import Codec.GlTF.Skin (Skin)
import Codec.GlTF.Texture (Texture)

-- | The root object for a glTF asset.
data GlTF = GlTF
  { asset              :: Asset
  , extensionsUsed     :: Maybe (Vector Text)
  , extensionsRequired :: Maybe (Vector Text)
  , accessors          :: Maybe (Vector Accessor)
  , animations         :: Maybe (Vector Animation)
  , buffers            :: Maybe (Vector Buffer)
  , bufferViews        :: Maybe (Vector BufferView)
  , cameras            :: Maybe (Vector Camera)
  , images             :: Maybe (Vector Image)
  , materials          :: Maybe (Vector Material)
  , meshes             :: Maybe (Vector Mesh)
  , nodes              :: Maybe (Vector Node)
  , samplers           :: Maybe (Vector Sampler)
  , scenes             :: Maybe (Vector Scene)
  , skins              :: Maybe (Vector Skin)
  , textures           :: Maybe (Vector Texture)
  , extensions         :: Maybe Object
  , extras             :: Maybe Value
  } deriving (Eq, Show, Generic)

instance FromJSON GlTF
instance ToJSON GlTF
