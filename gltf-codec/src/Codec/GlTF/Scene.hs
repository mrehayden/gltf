module Codec.GlTF.Scene
  ( SceneIx(..)
  , Scene(..)
  ) where

import Codec.GlTF.Prelude

import Codec.GlTF.Node (NodeIx)

newtype SceneIx = SceneIx { unSceneIx :: Int }
  deriving (Eq, Ord, Show, FromJSON, ToJSON, Generic)

data Scene = Scene
  { nodes       :: Maybe (Vector NodeIx)
  , name        :: Maybe Text
  , extensions  :: Maybe Object
  , extras      :: Maybe Value
  } deriving (Eq, Show, Generic)

instance FromJSON Scene
instance ToJSON Scene
